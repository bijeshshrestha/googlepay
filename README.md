Node project to test google pay msp you can go to localhost:3000/gpay

The code sample is in gpay.hbs file

1. onload="onGooglePayLoaded()" prepares the google pay button.
2. The google pay button, on click, registers a callback function for success and error handling by calling 
    onGooglePaymentButtonClicked() method.
3. The success response is handled in processPayment(paymentData) method.

P.S. For production environment, need to update merchant info in getGooglePaymentDataRequest() method
and const tokenizationSpecification. Change environment type in getGooglePaymentsClient() method.