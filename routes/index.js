var express = require('express');
//var path = require('path');
var router = express.Router();
var settings = require("../gpaySettings.json");
var title = "Google Pay Offer Object";
var MobileDetect = require('mobile-detect');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: title, OfferClass: settings.offerClass, showGPay: false });
});

router.get('/gift', function (req, res, next) {
  res.render('gift', { title: title, OfferClass: settings.offerClass, showGPay: false });
});

router.post('/postClass', function (req, res, next) {
  var offerClass = require("../offerClass");
  var offerObject = require("../offerObject");

  var params = settings;
  settings.layout = false;

  res.render('OfferClass', settings, function (err, json) {
    if (err) {
      console.log(err);
    }
    else {
      console.log(json);
      offerClass.saveClass(json);
    }

    var jwt = offerObject.generateJWT(req.headers.host);
    // var jwt="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiI4MTUwNTg4Njk5MS1jb21wdXRlQGRldmVsb3Blci5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiZ29vZ2xlIiwidHlwIjoic2F2ZXRvYW5kcm9pZHBheSIsImlhdCI6MTUzOTgwMTU1MiwicGF5bG9hZCI6eyJ3ZWJzZXJ2aWNlUmVzcG9uc2UiOnsicmVzdWx0IjoiYXBwcm92ZWQiLCJtZXNzYWdlIjoiU3VjY2Vzcy4ifSwibG95YWx0eUNsYXNzZXMiOltdLCJsb3lhbHR5T2JqZWN0cyI6W3sia2luZCI6IndhbGxldG9iamVjdHMjbG95YWx0eU9iamVjdCIsImNsYXNzSWQiOiIzMTAzNzEyNjc3NTI1NDY3NjU5LmFuZHJvaWRfcGF5X3Rlc3RfMF9wdW5jaGVzIiwiaWQiOiIzMTAzNzEyNjc3NTI1NDY3NjU5LjE3NTg3NzMzMDAzMDA4MDgwMDQiLCJhY2NvdW50SWQiOiIxNzU4NzczMzAwMzAwODA4MDA0IiwiYWNjb3VudE5hbWUiOiJQcmFmdWwiLCJiYXJjb2RlIjp7ImFsdGVybmF0ZVRleHQiOiIxNzU4NzczMzAwMzAwODA4MDA0IiwidHlwZSI6InBkZjQxNyIsInZhbHVlIjoiN0VsZXZlbkRHRTAwMUFOU0kgRDIwMDAwMDEwMDAxSUQwMDQxMDAyM0RHRTE3NTg3NzMzMDAzMDA4MDgwMDQifSwic3RhdGUiOiJhY3RpdmUiLCJ2ZXJzaW9uIjoxfV0sIm9mZmVyQ2xhc3NlcyI6W10sIm9mZmVyT2JqZWN0cyI6W119LCJvcmlnaW5zIjpbImh0dHBzOi8vYXBpLXRlc3QuNy1lbGV2ZW4uY29tIiwiaHR0cHM6Ly9zcnAtdGVzdC50ZXJtbi51cyJdfQ.ZUJxabOmE_Pk3hfoFnQyAZ1h4q_GkAYV7tA-BeiRHdZKb9pNqv2f0DP-y-SmGV7cdEG9pmjdF8YXIGKdwNlBXNQvljBe9YIjjLQhFJp2CYJveJovRWQbPyJh38RBfoTV-vLnXmKN41nJ99oXqZBOOz3Gk0b97n9oexJukvjO0GGgnniXuSj7hzWuBytOXQ3WJ2SxV1n5n61OKeAOYtG2RvlmmVIWfHnzIcEQLnQIxdqevBioCXuRArU7YiQBmw8poHr-gxbaBr42nnEI5kCWmUV3vIKLqM4vFSbkBs8pNdlO-nEJJx05iqStHyEaeq9Lp1pbyvou5t5nQlgDbGEy7Q";
    res.render('index', { title: title, OfferClass: "Submitted", jwt: jwt, showGPay: true });
  });

  //  res.render('index', { title: 'Express', OfferClass: "Submitted" });
});

router.post('/postGift', function (req, res, next) {
  var offerClass = require("../giftClass");
  var offerObject = require("../offerObject");

  var params = settings;
  settings.layout = false;

  res.render('GiftCardClass', settings, function (err, json) {
    if (err) {
      console.log(err);
    }
    else {
      console.log(json);
      offerClass.saveClass(json);
    }

    var jwt = offerObject.generateJWT(req.headers.host);
    // var jwt="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiI4MTUwNTg4Njk5MS1jb21wdXRlQGRldmVsb3Blci5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiZ29vZ2xlIiwidHlwIjoic2F2ZXRvYW5kcm9pZHBheSIsImlhdCI6MTUzOTgwMTU1MiwicGF5bG9hZCI6eyJ3ZWJzZXJ2aWNlUmVzcG9uc2UiOnsicmVzdWx0IjoiYXBwcm92ZWQiLCJtZXNzYWdlIjoiU3VjY2Vzcy4ifSwibG95YWx0eUNsYXNzZXMiOltdLCJsb3lhbHR5T2JqZWN0cyI6W3sia2luZCI6IndhbGxldG9iamVjdHMjbG95YWx0eU9iamVjdCIsImNsYXNzSWQiOiIzMTAzNzEyNjc3NTI1NDY3NjU5LmFuZHJvaWRfcGF5X3Rlc3RfMF9wdW5jaGVzIiwiaWQiOiIzMTAzNzEyNjc3NTI1NDY3NjU5LjE3NTg3NzMzMDAzMDA4MDgwMDQiLCJhY2NvdW50SWQiOiIxNzU4NzczMzAwMzAwODA4MDA0IiwiYWNjb3VudE5hbWUiOiJQcmFmdWwiLCJiYXJjb2RlIjp7ImFsdGVybmF0ZVRleHQiOiIxNzU4NzczMzAwMzAwODA4MDA0IiwidHlwZSI6InBkZjQxNyIsInZhbHVlIjoiN0VsZXZlbkRHRTAwMUFOU0kgRDIwMDAwMDEwMDAxSUQwMDQxMDAyM0RHRTE3NTg3NzMzMDAzMDA4MDgwMDQifSwic3RhdGUiOiJhY3RpdmUiLCJ2ZXJzaW9uIjoxfV0sIm9mZmVyQ2xhc3NlcyI6W10sIm9mZmVyT2JqZWN0cyI6W119LCJvcmlnaW5zIjpbImh0dHBzOi8vYXBpLXRlc3QuNy1lbGV2ZW4uY29tIiwiaHR0cHM6Ly9zcnAtdGVzdC50ZXJtbi51cyJdfQ.ZUJxabOmE_Pk3hfoFnQyAZ1h4q_GkAYV7tA-BeiRHdZKb9pNqv2f0DP-y-SmGV7cdEG9pmjdF8YXIGKdwNlBXNQvljBe9YIjjLQhFJp2CYJveJovRWQbPyJh38RBfoTV-vLnXmKN41nJ99oXqZBOOz3Gk0b97n9oexJukvjO0GGgnniXuSj7hzWuBytOXQ3WJ2SxV1n5n61OKeAOYtG2RvlmmVIWfHnzIcEQLnQIxdqevBioCXuRArU7YiQBmw8poHr-gxbaBr42nnEI5kCWmUV3vIKLqM4vFSbkBs8pNdlO-nEJJx05iqStHyEaeq9Lp1pbyvou5t5nQlgDbGEy7Q";
    res.render('gift', { title: title, OfferClass: "Submitted", jwt: jwt, showGPay: true });
  });

  //  res.render('index', { title: 'Express', OfferClass: "Submitted" });
});

router.post('/addMessage', function (req, res, next) {

  var offerMessage = require("../offerMessage");
  var offerObject = require("../offerObject");

  var params = settings;
  settings.layout = false;

  res.render('message', settings, function (err, json) {
    if (err) {
      console.log(err);
    }
    else {
      console.log(json);
      offerMessage.addMessage(json);
    }

    var jwt = offerObject.generateJWT(req.headers.host);
    res.render('index', { title: title, OfferClass: "Submitted", jwt: jwt, showGPay: false });
  });

});

router.get('/device', function (req, res, next) {
  md = new MobileDetect(req.headers['user-agent']);
  console.log(md.mobile());          // 'Sony'
  console.log(md.phone());           // 'Sony'
  console.log(md.tablet());          // null
  console.log(md.userAgent());       // 'Safari'
  console.log(md.os());              // 'AndroidOS'
  console.log(md.is('iPhone'));      // false
  console.log(md.is('bot'));         // false
  console.log(md.version('Webkit'));         // 534.3
  console.log(md.versionStr('Build'));       // '4.1.A.0.562'
  console.log(md.match('playstation|xbox')); // false
  console.log(JSON.stringify(md));

  // console.log(req.headers);
  
  res.render('device', { device: JSON.stringify(md), header: JSON.stringify(req.headers) });
});

router.get('/gpay', function (req, res, next) {  
  res.render('gpay', {});
});

module.exports = router;
